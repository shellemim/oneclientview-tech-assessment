# OneClientView Technical Assessment
### Author: Michelle Leong Hwee-Ling
## !Important Note!
This repository is unfinished, as during development I was unable to overcome a critical issue that was preventing me from communicating with my database. Additionally, due to time constraints, I was unable to continue investing time into developing this Rest API in GoLang. This repository therefore contains my partial attempt to create the required API. However, I have managed to completed the assessment requirements using a Flask API in [**this repository**](https://gitlab.com/shellemim/oneclientview-tech-assessment-2), complete with CI integration. I hope you will take this into consideration when evaluating my competency as a candidate in developing Rest APIs.
## Description
This project is an REST API interface, with the aim of providing the backend integration for teachers to access and manage their student's information. This application was developed as part of the GovTech OneClientView Technical Assessment.

As this Rest API is very isolated and lightweight, a single microservice approach was taken. Student data is stored, processed and queries within our students-microservice, which uses a MySQL database, and is containerized and hosted on ECS. This microservices approach improves the functionality of the service by allowing the Rest API to be more easily scalable, more reliable, and have higher availability.

## File Directory
<!-- trunk-ignore(markdownlint/MD040) -->
```
.
├── services
│   └── students-microservice       # Microservice for managing students
│       ├── src
|       |   ├── main.go             # Main source code
|       |   ├── go.mod
|       |   └── go.sum
│       └── Dockerfile              # Dockerfile for students microservice
├── docker-compose.yml              # Docker Compose yml
└── ...
```
## Usage
### Running Locally
This repository can also be cloned run locally on your own machine. Please refer to the [Installation](#installation) section for a guide on how to set-up this Rest API on your local environment.

### Database Structure
In order to keep in line with the microservices approach, the database was created to house only student data (Email, Suspended status). However, our requirements dictate that each Student must be associated with one or more teacher. This would require a relational database, however, as we are taking on a microservices approach, we should only deal with one entity per microservice. As such, we use an ID in order to keep our rows unique, as well as verify that no entries exist with the same student email and teacher email when creating new student records.

As such, here is our final database structure:

| **id** | email | suspended | teacher |
| --- | --------- | -------- | --------|
| Integer | String | Boolean | String |

### Unit Tests

## Installation
1. Ensure you have [Docker](https://docs.docker.com/engine/install/) installed
2. CD into the main directory (/oneclientview-tech-assessment)
3. Run "docker compose up -d --build" in the command line
4. App will be run on localhost:9090
## Project Management
Due to the tight timeline of this assessment, it was crucial to use projet management tools to keep the project on track, and ensure no requirements/components of the assessments were missed during the development of the application. As such, a KanBan board was used to keep track of all the tasks to be completed.

![Kanban Board](/readme_res/Kanban.png "Kanban Board")
![Kanban Board Card](/readme_res/Card.png "Kanban Board Card")

During the initial project planning, the tasks identified from the requirements were as follows (in order of planned succession):
1. Create Git Repository & Base Application
2. Dockerization
3. Set up & Connect AWS RDS MySQL Server
4. Develop Rest APIs
5. Write Test Cases
6. Host finished Rest API Application
7. Write Documentation & Code Clean-Up

## Acknowledgments
Since this was my first time working with Golang, a big thank you to these resources:
- [Go in 100 Seconds](https://www.youtube.com/watch?v=446E-r0rXHI) for an introduction to Golang
- [How to Install GoLang](https://www.geeksforgeeks.org/how-to-install-golang-on-macos/) for installation and set up instructions
- [Build a Rest API with GoLang](youtube.com/watch?v=d_L64KT3SFM) for an easy, beginner-friendly step-by-step intro and explanation for creating a Rest API with Go
- [Build your Go image](https://docs.docker.com/language/golang/build-images/) for building the Dockerfile
- [Go MySQL Beginners Tutorial](https://www.youtube.com/watch?v=DWNozbk_fuk) for integration the MySQL Database
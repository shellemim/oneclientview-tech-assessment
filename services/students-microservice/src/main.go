package main

import (
	"database/sql"
	"fmt"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
)

// Function for loading variables from .env
func getEnvVariable(key string) string {
    return os.Getenv(key)
}

type Student struct {
    Id          int     `json:"id"`
    Email       string  `json:"email"`
    Suspended   int     `json:"status"`
    Teacher     string  `json:"teacher"`
}

type ErrMsg struct {
    ErrorCode   int
    Message     string
}

func healthcheck(context *gin.Context) {
    context.IndentedJSON(http.StatusOK, gin.H{"status": 200, "message": "Students Service is Healthy" , "alive": true})
}

// ! Causing errors
func getStudents(context *gin.Context) {
    var students []Student

    results, err := db.Query("SELECT * FROM students;")
    if err != nil  {
        panic(err.Error())
    }

    for results.Next() {
        var student Student

        err = results.Scan(&student.Id, &student.Email, &student.Suspended, &student.Teacher)
        if err != nil {
            panic(err.Error())
        }

        students = append(students, student)
    }

    context.IndentedJSON(http.StatusOK, students)
}

var db *sql.DB

func main() {
    // Connect to database
    db, err := sql.Open("mysql", getEnvVariable("db_conn"))
    if err != nil {
        fmt.Println("Error: Unable to connect to MySQL db")
        panic(err.Error())
    }
    defer db.Close()
    fmt.Println("Successfully connected to MySQL db")

	// Test query (this works)
    results, err := db.Query("SELECT * FROM students;")
    if err != nil {
        panic(err.Error())
    }

    for results.Next() {
        var student Student

        err = results.Scan(&student.Id, &student.Email, &student.Suspended, &student.Teacher)
        if err != nil {
            panic(err.Error())
        }

        fmt.Println(student.Email)
    }

    router := gin.Default()

    router.GET("/students/health", healthcheck)
    router.GET("/students/all", getStudents)
    // router.GET("/students/register", registerStudents)
    // router.GET("/studenmts/commonstudents", getCommonStudents)
    // router.GET("/students/suspend", suspendStudent)
    // router.GET("/students/retrievefornotifications", notifyStudents)

    router.Run(":9090")
}